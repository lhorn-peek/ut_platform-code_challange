# UT_Platform


## Getting started

UT_Platform is an Elixir 1.6 application using PostgreSQL for persistence.

### Prerequisites

You must install the following dependencies before starting:

- [Git](https://git-scm.com/).
- [Elixir](https://elixir-lang.org/install.html) (v1.6 or later).
- [PostgreSQL](https://www.postgresql.org/) database (v9.5 or later).

### Configuring Conduit

1. Clone the Git repo from GitLab

    ```console
    $ git clone <ADD GIT URL HERE>
    ```

2. Install mix dependencies:

    ```console
    $ cd ut_platform
    $ mix deps.get
    ```

3. Create the event store database:

    ```console
    $ mix do setup_es
    ```

4. Compile:

    ```console
    $ mix compile
    ```

### Running the tests

1. reset the event store 

    ```console
    $ mix do reset_es
    ```

2. run test with --no-start flag

    ```console
    $ mix test --no-start
    ```
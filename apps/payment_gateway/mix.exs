defmodule PaymentGateway.MixProject do
  use Mix.Project

  def project do
    [
      app: :payment_gateway,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.6",
      elixirc_paths: elixirc_paths(Mix.env),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  def application do
    [
      extra_applications: [:logger, :eventstore],
      mod: {PaymentGateway.Application, []}
    ]
  end
  
  defp elixirc_paths(:test), do: ["lib", "test/utils"]
  defp elixirc_paths(:dev), do: ["lib", "test/utils"]
  defp elixirc_paths(_),     do: ["lib"]
  

  defp deps do
    [
      {:commanded, "~> 0.15"},
      {:commanded_eventstore_adapter, "~> 0.3"},
      {:transaction_verification, in_umbrella: true},
      {:postgrex, ">= 0.0.0"},
      {:uuid, "~> 1.1"}
    ]
  end

  defp aliases do
    [
      setup_es: ["event_store.create", "event_store.init"],
      reset_es: ["event_store.drop", "setup_es"]
    ]
  end
end

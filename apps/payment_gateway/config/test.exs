use Mix.Config

config :commanded,
  event_store_adapter: Commanded.EventStore.Adapters.InMemory

  config :eventstore, EventStore.Storage,
    serializer: Commanded.Serialization.JsonSerializer,
    username: "postgres_",
    password: "postgres",
    database: "eventstore_dev",
    hostname: "localhost",
    pool_size: 10
defmodule PaymentGateway.Integrations.ConfirmTransaction do
  use Commanded.Event.Handler, name: "Id.PaymentGateway.Integrations.ConfirmTransaction"
  alias PaymentGateway.Router
  alias PaymentGateway.Events.{CryptoPaymentReceived}
  alias PaymentGateway.Commands.{AcceptConfirmationSuccess}
  alias TransactionVerification.ServiceInterface.ConfirmTnxDto
  alias TransactionVerification.ServiceInterface, as: ConfirmationService
  require Logger
  
  @confirmations_required 2


  def handle(%CryptoPaymentReceived{} = event, _metadata) do
    dto = map_dto(event)
    :started = ConfirmationService.confirm(dto)
    :ok
  end

  defp map_dto(event) do
    %ConfirmTnxDto{
      currency: event.currency,
      tnx_hash: event.transaction_hash,
      reference_id: event.payment_id,
      callback_mod: __MODULE__,
      callback_func: :confirmation_callback,
      num_confirmations: @confirmations_required
    }
  end

  def confirmation_callback(%ConfirmTnxDto{outcome: "success"} = res) do
    Logger.info "received confirmation success..."
    :ok = 
      %AcceptConfirmationSuccess{payment_id: res.reference_id}
      |> Router.dispatch()
  end

end

defmodule PaymentGateway.Payment do
  require Logger
  defstruct payment_id: nil,
            currency: nil,
            amount: nil,
            target_currency: nil,
            target_currency_amount: nil,
            recipient_id: nil,
            account_id: nil,
            transaction: nil,
            status: "new",
            sender: "guest",
            sender_id: nil

  alias __MODULE__
  alias PaymentGateway.Payment.Status
  alias PaymentGateway.Commands.{ReceiveCryptoPayment, AcceptConfirmationSuccess}
  alias PaymentGateway.Events.{CryptoPaymentReceived, ConfirmationAccepted}

  @doc """
  Make a cryptocurrency payment to a UTrust account.
  """
  def execute(%Payment{payment_id: nil}, %ReceiveCryptoPayment{} = cmd) do
    %CryptoPaymentReceived{
      payment_id: cmd.payment_id,
      account_id: cmd.account_id,
      currency: cmd.currency,
      transaction_hash: cmd.transaction_hash
    }
  end

  @doc """
  Handle the result for a succesful transaction confirmation
  """
  def execute(%Payment{} = state, %AcceptConfirmationSuccess{} = cmd) do
    %ConfirmationAccepted{
      payment_id: cmd.payment_id
    }
  end

  # state mutators

  def apply(_state, %CryptoPaymentReceived{} = event) do
    %Payment{
      payment_id: event.payment_id,
      account_id: event.account_id,
      transaction: event.transaction_hash,
      currency: event.currency,
      status: Status.processing
    }
  end

  def apply(%Payment{} = state, %ConfirmationAccepted{} = event) do
    Logger.info "'Payment' has received event 'ConfirmationAccepted'"
    %Payment{state | 
      status: Status.verified
    }
  end
end

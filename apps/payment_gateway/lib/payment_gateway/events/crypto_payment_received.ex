defmodule PaymentGateway.Events.CryptoPaymentReceived do
  defstruct payment_id: nil,
            account_id: nil,
            currency: nil,
            transaction_hash: nil
end

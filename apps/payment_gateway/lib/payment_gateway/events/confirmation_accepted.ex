defmodule PaymentGateway.Events.ConfirmationAccepted do
  defstruct payment_id: nil
end

defmodule PaymentGateway.Application do
  @moduledoc false
  
  use Application
  alias PaymentGateway.Integrations.ConfirmTransaction

  def start(_type, _args) do
    import Supervisor.Spec
    
    children = [
      worker(ConfirmTransaction, [[start_from: :origin]], id: :confirm_transaction_integration)
    ]

    opts = [strategy: :one_for_one, name: PaymentGateway.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

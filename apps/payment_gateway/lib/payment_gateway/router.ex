defmodule PaymentGateway.Router do
  use Commanded.Commands.Router

  alias PaymentGateway.Payment
  alias PaymentGateway.Commands.{ReceiveCryptoPayment, AcceptConfirmationSuccess}

  identify Payment, by: :payment_id

  dispatch [
    ReceiveCryptoPayment,
    AcceptConfirmationSuccess
  ], to: Payment
end

defmodule PaymentGateway.Commands.ReceiveCryptoPayment do
  defstruct payment_id: nil,
            account_id: nil,
            currency: nil,
            transaction_hash: nil
end

defmodule PaymentGateway.Commands.AcceptConfirmationSuccess do
  defstruct payment_id: nil
end

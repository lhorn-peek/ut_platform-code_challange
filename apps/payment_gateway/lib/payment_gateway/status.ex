defmodule PaymentGateway.Payment.Status do
  def processing, do: "processing_payment"

  def verifying, do: "verification_pending"
  def verified, do: "verification_success"
  def un_verified, do: "verification_failed"

  def complete, do: "payment_complete"
end

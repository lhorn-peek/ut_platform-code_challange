defmodule PaymentGatewayTest do
  use InMemoryEventStoreCase
  import Commanded.Assertions.EventAssertions
  alias PaymentGateway.Router
  alias PaymentGateway.Events.{CryptoPaymentReceived, ConfirmationAccepted}
  alias PaymentGateway.Commands.{ReceiveCryptoPayment}


  test "receives event 'CryptoPaymentReceived' when payment is made" do
    :ok = 
      %ReceiveCryptoPayment{
        currency: "eth",
        payment_id: "id-payment-123",
        account_id: "id-merchant-xyz",
        transaction_hash: TnxHash.eth,
      }
      |> Router.dispatch()

    assert_receive_event(
      CryptoPaymentReceived, 
      fn event -> assert(event.payment_id == "id-payment-123") end
    )
  end

  test "successful tnx confirmation is accepted after processing" do
    :ok = 
      %ReceiveCryptoPayment{
        currency: "eth",
        payment_id: "id-payment-456",
        account_id: "id-merchant-abcz",
        transaction_hash: TnxHash.eth,
      }
      |> Router.dispatch()
  
    wait_for_event(
      ConfirmationAccepted, 
      fn accepted -> accepted.payment_id == "id-payment-456" end
    )
  end
end



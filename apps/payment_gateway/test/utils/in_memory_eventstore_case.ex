defmodule InMemoryEventStoreCase do
  require Logger
  use ExUnit.CaseTemplate

  setup do

    {:ok, event_store} = Commanded.EventStore.Adapters.InMemory.start_link()

    Application.ensure_all_started(:payment_gateway)

    Logger.info "setting up 'InMemoryEventStoreCase' for test use..."

    on_exit fn ->
     Application.stop(:payment_gateway)

      shutdown(event_store)
    end

    :ok
  end

  def shutdown(pid) when is_pid(pid) do
    Process.unlink(pid)
    Process.exit(pid, :shutdown)

    ref = Process.monitor(pid)
    assert_receive {:DOWN, ^ref, _, _, _}, 5_000
  end
end
defmodule ATest do
  require Logger

  # [CODE SCRATCH PAD] 


  # ~ transaction verification

  alias TransactionVerification.ServiceInterface
  alias TransactionVerification.ServiceInterface.ConfirmTnxDto

  def transaction_verification do
    dto = %ConfirmTnxDto{
      currency: "eth",
      tnx_hash: "0x7145b7006576c7e9970c2f690e1392a3774013cec17da5e01d3430c116a0b2ea",
      reference_id: "123",
      callback_mod: ATest,
      callback_func: :transaction_verification_callback,
      num_confirmations: 2
    }

    ServiceInterface.confirm(dto)
  end

  def transaction_verification_callback(%ConfirmTnxDto{} = res) do
    Logger.info("result received:")
    IO.inspect(res)
  end

  # ~ payment

  alias PaymentGateway.Router
  alias PaymentGateway.Commands.ReceiveCryptoPayment

  def receive_payment do
      %ReceiveCryptoPayment{
        payment_id: UUID.uuid4,
        account_id: "merchant-acc-id-123",
        currency: "eth",
        transaction_hash: "0xda07764653a2732755716c507d5dd37061aad40efad318d3f086a3d47993af28"
      } 
      |> Router.dispatch()
  end
end
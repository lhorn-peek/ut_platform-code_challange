defmodule TransactionVerification.ServiceInterface do
  alias TransactionVerification.Crypto.Ethereum
  alias TransactionVerification.ServiceInterface.ConfirmTnxDto

  def confirm(%ConfirmTnxDto{currency: "eth"} = dto) do
    :ok = Ethereum.start(dto)
    :started
  end
end




defmodule TestConf do
  require Logger
  alias TransactionVerification.ServiceInterface
  alias TransactionVerification.ServiceInterface.ConfirmTnxDto

  def test do
    dto = %ConfirmTnxDto{
      currency: "eth",
      tnx_hash: "0x7145b7006576c7e9970c2f690e1392a3774013cec17da5e01d3430c116a0b2ea",
      reference_id: "123",
      callback_mod: TestConf,
      callback_func: :callback,
      num_confirmations: 2
    }

    ServiceInterface.confirm(dto)
  end

  def callback(%ConfirmTnxDto{} = res) do
    Logger.info("result received:")
    IO.inspect(res)
  end
end
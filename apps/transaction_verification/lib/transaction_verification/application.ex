defmodule TransactionVerification.Application do
  @moduledoc false

  use Application

  def start(_type, _args) do

    children = [
      # Starts a worker by calling: TransactionVerification.Worker.start_link(arg)
      # {TransactionVerification.Worker, arg},
    ]

    opts = [strategy: :one_for_one, name: TransactionVerification.Supervisor]
    Supervisor.start_link(children, opts)
  end
end

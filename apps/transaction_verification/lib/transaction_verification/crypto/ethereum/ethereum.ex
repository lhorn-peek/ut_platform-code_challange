defmodule TransactionVerification.Crypto.Ethereum do
  use Task, restart: :temporary
  require Logger
  alias Etherscan.API.Proxy
  alias Etherscan.ProxyTransaction, as: ProxyTnx
  alias TransactionVerification.ServiceInterface.ConfirmTnxDto

  @delay 1000

  def start(%ConfirmTnxDto{} = opts) do
    {:ok, pid} = Task.Supervisor.start_link()
    Task.Supervisor.start_child(pid, TransactionVerification.Crypto.Ethereum, :run, [opts])
    :ok
  end

  def run(%ConfirmTnxDto{} = opts) do
    Logger.info("calling etherscan...")
    %ConfirmTnxDto{tnx_hash: hash, num_confirmations: required} = opts
    count = get_confirmations(hash)

    if count >= required do
      Logger.info("tnx confirmed!")
      finish(opts)
    else
      Process.sleep(1000)
      run(opts)
    end
  end

  def get_confirmations(tnx_hash) do
    {:ok, transaction} = Proxy.eth_get_transaction_by_hash(tnx_hash)
    
    %ProxyTnx{blockNumber: block_num} = transaction
    calc_confirmations(block_num)
  end

  defp calc_confirmations(tnx_block) do
    {:ok, current_block} = Proxy.eth_block_number()
    Logger.info "tnx_block: #{tnx_block} | current_block: #{current_block}"

    current_block - tnx_block
  end

  def finish(dto) do
    result = %ConfirmTnxDto{dto | outcome: "success"}
    apply(result.callback_mod, result.callback_func, [result])
  end
end

defmodule TransactionVerification.ServiceInterface.ConfirmTnxDto do
  @doc """
  This struct is used to provide details of a 'Cryptocurrency Transaction' 
  to the confirmation service.
  """
  defstruct [
    :currency,
    :tnx_hash,
    :reference_id,
    :callback_mod,
    :callback_func,
    :num_confirmations,
    # :success / :failed
    :outcome
  ]
end
